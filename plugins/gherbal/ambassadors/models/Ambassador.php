<?php namespace Gherbal\Ambassadors\Models;

use Model;

/**
 * Model
 */
class Ambassador extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'gherbal_ambassadors_';

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = ['excerpt','content'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
