<?php namespace Gherbal\Ambassadors\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGherbalAmbassadors extends Migration
{
    public function up()
    {
        Schema::table('gherbal_ambassadors_', function($table)
        {
            $table->string('name');
            $table->string('slug');
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
        });
    }
    
    public function down()
    {
        Schema::table('gherbal_ambassadors_', function($table)
        {
            $table->dropColumn('name');
            $table->dropColumn('slug');
            $table->string('first_name', 191);
            $table->string('last_name', 191);
        });
    }
}
