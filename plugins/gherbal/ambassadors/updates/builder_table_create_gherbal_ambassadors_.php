<?php namespace Gherbal\Ambassadors\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGherbalAmbassadors extends Migration
{
    public function up()
    {
        Schema::create('gherbal_ambassadors_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('excerpt', 255);
            $table->text('content');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('gherbal_ambassadors_');
    }
}
