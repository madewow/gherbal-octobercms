# README #

###  Herbal October CMS setup ###

* Run installer.php to download October CMS files and follow instructions to set it up
* Front Settings / Updates & Plugins search and install the following:
..* Builder plugin
..* Drivers plugin
..* Rainlab Blog plugin
..* Rainlab Translate plugin

* Copy 'gherbal' folder in themes from repo to server

* Copy 'gherbal' folder in plugins from repo to server

* Run php artisan october:up to migrate gherbal plugin database table

* activate the gherbal theme from Settings / CMS / Front-end theme